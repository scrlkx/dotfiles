# dotfiles

My personal set of dotfiles and settings stuff.

## What's inside

There are several cool things like:

- Git setup (with mixed settings support)
- Z Shell, Antigen and Alacritty setup 
- Custom aliases
- Sublime Text settings

And more!

## Before install

To make a full use of dotfiles you will need to install the following softwares: 
1. [git][]
2. [diff-so-fancy][] 
3. [Z Shell][]
4. [Oh My Zsh][]
5. [Alacritty][]
6. [Sublime Text][]

## Getting started

To setup your dotfiles:

1. Clone this repository
2. Execute the setup `./setup`

[git]: https://git-scm.com
[diff-so-fancy]: https://github.com/so-fancy/diff-so-fancy
[Z Shell]: https://www.zsh.org
[Oh My Zsh]: https://github.com/ohmyzsh/ohmyzsh/wiki
[Alacritty]: https://alacritty.org
[Sublime Text]: https://www.sublimetext.com
